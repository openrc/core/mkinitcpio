# Maintainer: OpenStage linux <openstagelinux@gmail.com>

pkgname=mkinitcpio-os
pkgver=31
pkgrel=2
pkgdesc="Modular initramfs image creation utility"
arch=('any')
url='https://github.com/archlinux/mkinitcpio'
license=('GPL')
groups=(os-core)
depends=('awk' 'mkinitcpio-busybox>=1.19.4-2' 'kmod' 'util-linux>=2.23' 'libarchive' 'coreutils'
         'bash' 'binutils' 'diffutils' 'findutils' 'grep' 'filesystem-os' 'zstd' 'udev')
makedepends=('asciidoc')
optdepends=('gzip: Use gzip compression for the initramfs image'
            'xz: Use lzma or xz compression for the initramfs image'
            'bzip2: Use bzip2 compression for the initramfs image'
            'lzop: Use lzo compression for the initramfs image'
            'lz4: Use lz4 compression for the initramfs image'
            'mkinitcpio-nfs-utils: Support for root filesystem on NFS')
provides=('initramfs' 'mkinitcpio')
replaces=('mkinitcpio')
conflicts=('mkinitcpio')
backup=('etc/mkinitcpio.conf')
source=("https://sources.archlinux.org/other/${pkgname%-*}/${pkgname%-*}-$pkgver.tar.gz"
        nosystemd.patch)
install=mkinitcpio.install

md5sums=('e7271c7c01dfa44afab6a3e0cccdcb51'
         '653214fc53c36def803bb79422c4ee80')

prepare() {
  cd ${pkgname%-*}-$pkgver
  patch -Np1 -i ../nosystemd.patch
}

check() {
  make -C "${pkgname%-*}-$pkgver" check
}

package() {
  make -C "${pkgname%-*}-$pkgver" DESTDIR="$pkgdir" install
  rm -f "${pkgdir}"/usr/lib/initcpio/install/sd-*
}
